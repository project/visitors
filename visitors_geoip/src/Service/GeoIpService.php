<?php

namespace Drupal\visitors_geoip\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\visitors_geoip\VisitorsGeoIpInterface;
use GeoIp2\Database\Reader;

/**
 * GeoIp lookup Service.
 *
 * @package visitors
 */
class GeoIpService implements VisitorsGeoIpInterface {

  /**
   * The GeoIP reader.
   *
   * @var \GeoIp2\Database\Reader
   */
  protected $reader;

  /**
   * Constructs a new GeoIpService object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $settings = $config_factory->get('visitors_geoip.settings');
    $path = $settings->get('geoip_path');
    $free_database = $path . '/GeoLite2-City.mmdb';
    $better_database = $path . '/GeoIP2-City.mmdb';
    $database = NULL;
    if (file_exists($better_database)) {
      $database = $better_database;
    }
    elseif (file_exists($free_database)) {
      $database = $free_database;
    }

    if ($database) {
      $this->reader = new Reader($database);
    }

  }

  /**
   * {@inheritdoc}
   */
  public function metadata() {
    if (is_null($this->reader)) {
      return NULL;
    }
    $metadata = $this->reader->metadata();
    return $metadata;
  }

  /**
   * {@inheritdoc}
   */
  public function city($ip_address) {
    if (is_null($this->reader)) {
      return NULL;
    }
    $record = $this->reader->city($ip_address);
    return $record;
  }

  /**
   * {@inheritdoc}
   */
  public function getReader() {
    return $this->reader;
  }

  /**
   * {@inheritdoc}
   */
  public function setReader($reader) {
    $this->reader = $reader;
  }

}
