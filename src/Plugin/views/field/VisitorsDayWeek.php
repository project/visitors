<?php

namespace Drupal\visitors\Plugin\views\field;

/**
 * Field handler to display the hour (server) of the visit.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("visitors_day_of_week")
 */
final class VisitorsDayWeek extends VisitorsTimestamp {

  /**
   * {@inheritdoc}
   */
  protected $format = '%w';

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    $weekdays = [
      0 => t('Sunday'),
      1 => t('Monday'),
      2 => t('Tuesday'),
      3 => t('Wednesday'),
      4 => t('Thursday'),
      5 => t('Friday'),
      6 => t('Saturday'),
    ];

    $value = (int) $this->getValue($values);

    return $weekdays[$value];
  }

}
