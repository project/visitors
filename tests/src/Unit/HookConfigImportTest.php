<?php

namespace Drupal\Tests\visitors\Unit;

use Drupal\Tests\UnitTestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;

require_once __DIR__ . '/../../../visitors.module';

/**
 * Tests hook_config_import.
 *
 * @group visitors
 */
class HookConfigImportTest extends UnitTestCase {

  /**
   * The cache invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $cacheInvalidator;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $container = new ContainerBuilder();
    $this->cacheInvalidator = $this->createMock('Drupal\Core\Cache\CacheTagsInvalidatorInterface');
    $container->set('cache_tags.invalidator', $this->cacheInvalidator);
    \Drupal::setContainer($container);
  }

  /**
   * @covers visitors_config_import
   * @dataProvider providerTestHookConfigImport
   */
  public function testHookConfigImport($config_name, $operation, $cache) {
    $this->cacheInvalidator->expects($this->exactly($cache))
      ->method('invalidateTags')
      ->with(['config:visitors.config']);

    visitors_config_import($config_name, $operation);
  }

  /**
   * Data provider for testHookConfigImport.
   */
  public static function providerTestHookConfigImport() {
    return [
      ['visitors.config', 'create', 0],
      ['visitors.config', 'delete', 0],
      ['visitors.config', 'import', 1],
      ['visitors.config', 'other', 0],
      ['visitors.config', 'save', 1],
      ['visitors.config', 'update', 0],
      ['system.config', 'create', 0],
      ['system.config', 'delete', 0],
      ['system.config', 'import', 0],
      ['system.config', 'other', 0],
      ['system.config', 'save', 0],
      ['system.config', 'update', 0],
    ];
  }

}
